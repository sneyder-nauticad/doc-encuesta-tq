## Indice Preguntas
* [SI/NO](questions/q-si-no.md)
* [Abierta](questions/qopen/README.md)
* [Dropdown](questions/qselect/README.md)
* [Seleccion Multiple](questions/qoptions/README.md)
* [Archivo](questions/q-file/README.md)
* [Ubicación](questions/q-geolocation/README.md) 
* [Porcentaje](questions/q-percentage/README.md) 
* [Ordenar](questions/q-sort/README.md) 
* [Multiples SI/NO](questions/q-multiple-toggle/README.md) 
* [Asignar Escala](questions/q-multiple-scale/README.md) 
* [Multiples Escalas](questions/q-scale/README.md) 
* [Multiples opciones compuesta](questions/q-composed/README.md)


### Grupos de preguntas
```
{
    "sections": number,
    "messages":{
        [section:number]: Mensaje
    }
}
```