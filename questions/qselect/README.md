## Dropdown
**Settings**
```
{
  options: [
    {
      text: Etiqueta,
      open?: valor opcional que indica si se debe abrir un campo abierto,
      label?: Etiqueta asociada con el campo abierto ,
      type?: Tipo del campo abierto (number, text),
    }, ...
  ],
  section: 1
}
```

**Respuesta**
```
text
```

[Volver](../../README.md)
