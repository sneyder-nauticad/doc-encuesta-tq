## Pregunta de Multiple Toggle


**Settings**

```
{
  "options": [
    {
      "text": Etiqueta
    }, ...
  ],
  "section": seccion a la que pertenece
}
```

**Respuesta**

Objeto serializado `JSON.parse([...])`
```
[{text: Etiqueta, value: 'SI'|'NO'}, ...]
```

[Volver](../../README.md)
