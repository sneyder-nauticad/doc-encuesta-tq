## Pregunta de porcentaje
El objetivo de esta pregunta es capturar n valores que sumen en total 100

**Settings**
```
{
  options: [{
    text: Etiqueta que se va a mostrar al usuario
  }, ...],
  section: Sección a la que pertenece con respecto a el grupo de preguntas
}
```

**Respuesta**

Un objeto serializado `JSON.parse({...})` con la siguiente estructura
```
[{
  text: Etiqueta mostrada al usuario,
  value: Valor numerico que selecciono el usuario
}, ...]
``` 

[Volver](../../README.md)
