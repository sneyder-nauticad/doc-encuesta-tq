## Multiples Escalas

**Settings**
```
{
  scales: [
    {
      label: Etiqueta,
      min: {
          label: Etiqueta,
          value: numero
      },
      max: {
         label: Etiqueta,
         value: numero
       },
      step: salto  
    }, ...
  ]
}
```

**Respuesta**

Objeto serializado
```
[{
  label: string, value: number
}, ...]
```

[Volver](../../README.md)
