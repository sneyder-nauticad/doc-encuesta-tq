## Multiples Opciones Compuestas

**Settings**
```
{
  options: [
    {
      text: Etiqueta,
      order?: numero,
      fields?: [
        {
          text: Etiqueta,
          type: number|text
        }, ...
      ]  
    }, ...
  ],
  multiple: boolean,
  section: Seccion a la que pertenece
}
```

**Respuesta**

Objeto serializado de las opciones seleccionadas
```
[
    {
      text: Etiqueta,
      fields: [
        {
          text: Etiqueta,
          value: string
        }, ...
      ]  
    }, ...
]
```

[Volver](../../README.md)
