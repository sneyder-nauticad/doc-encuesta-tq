## Pregunta de Selección múltiple
Seleccionar una o varias opciones de una lista, adicional una opcion puede accionar un campo de texto

**Parametros de configuración**

En el modelo de base de datos se debe asignar la siguiente configuración en el campo *settings*

``` javascript
{ 
  options: [
    {
      text: Etiqueta que se muestra al usuario
    }, 
    {
      open?: valor opcional que indica si se debe abrir un campo abierto,
      label?: Etiqueta asociada con el campo abierto ,
      type?: Tipo del campo abierto (number, text),
      align?: right | left,
      text: Etiqueta que se muestra al usuario
     }, ...
   ], 
   multiple: valor booleano que indica si puede seleccionar varias opciones,
   count?: cantidad a seleccionar, 
   show_if_parent_answer_is: Respuesta que debe tener el padre para que se pueda mostrar,
   section: Sección a la que pertenece 
}
```

[Volver](../../README.md)
