## Pregunta de archivo

Obtener un archivo

**Settings**

```
{
  accept: Tipo de archivos que se debe aceptar (image/*),
  section: Sección
}
```

**Respuesta**
```
Ruta de archivo
```
*Para este caso especifico de respuesta existe un modelo que almacena el archivo y guarda relación (fk) al modelo respuesta* 

[Volver](../../README.md)
