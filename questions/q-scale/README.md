## Pregunta de Escala

**Settings**

```
{
  "min": {
    label: Etiqueta,
    value: numero
  },
  "max": {
     label: Etiqueta,
     value: numero
   },
  "step": salto
}
```

**Respuesta**

Valor numerico obtenido en la escala

```
value
```

[Volver](../../README.md)
