## SI/No (Toggle)

**Settings**
```
{
    section: numero de la sección a la que pertenece,
    inline?: boolean (¿el enunciado y el toggle se debe mostrar en la misma linea?),
    show_if_parent_answer_is: 'SI'|'NO'
}
```
