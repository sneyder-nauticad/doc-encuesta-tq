## Geolocalización

**Settings**
```
{
  section: number
}
```

**Respuesta**

Objeto serializado
```
{
  "lat": number,
  "lng": number
}
```

[Volver](../../README.md)
