## Pregunta de Ordenamiento
Ordenar las opciones disponibles

**Settings**

```
{
  "options": [
    {
      "text": Etiqueta
    }, ...
  ],
  "section": seccion a la que pertenece
}
```

**Respuesta**

Objeto serializado ordenado`JSON.parse([...])`
```
[{text: Etiqueta},...]
```

[Volver](../../README.md)
